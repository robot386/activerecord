-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 26 Lis 2017, 12:34
-- Wersja serwera: 5.6.28
-- Wersja PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `myshop`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Identyfikator kategorii',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nazwa produktu',
  `description` text COLLATE utf8_unicode_ci COMMENT 'Opis produktu',
  `price` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT 'Cena produktu',
  `images` text COLLATE utf8_unicode_ci COMMENT 'Zdjęcia produktu zapisane w json - kilka zdjęć',
  `sku` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Numer magazynowy',
  `counter` int(11) NOT NULL DEFAULT '0' COMMENT 'Dostępna ilość produktu',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `price`, `images`, `sku`, `counter`, `created_at`, `updated_at`) VALUES
(1, 0, 'Acer Predator 17 X', '<b>Processor:</b> Intel Core i7-7820HK Quad-Core 2.9GHz, 8MB Cache (Turbo boost up to 3.9 GHz)\r\n\r\n<b>RAM:</b> 32GB DDR4 2400MHz RAM\r\n\r\n<b>Storage:</b> 1TB 7200RPM Hard Drive + 512GB Solid State Drive\r\n\r\n<b>Graphics:</b> NVIDIA GeForce GTX 1080 8GB DDR5 Graphics\r\n\r\n<b>Display:</b> 17.3 Inch Full HD Widescreen IPS Display with NVIDIA G-SYNC Technology\r\n\r\n<b>Operating System:</b> Windows 10 Home\r\n\r\n<b>Battery Life:</b> 3 hours\r\n\r\n<b>Weight:</b> 10 lbs\r\n\r\n<b>Upgrading Options:</b> RAM - Up to 64GB | Storage upgradable | CPU overclockable', '1079.1000', 'https://laptopunderbudget.com/wp-content/uploads/2017/06/Acer-Predator-17-X-Gaming-Laptop.jpg', 'MS3CV1', 4, '2017-11-25 20:56:05', '2017-11-26 14:41:34'),
(2, 0, 'Alienware 13 R3 13.3-Inch Gaming Laptop', '<b>Processor:</b> Intel Core i7-7700HQ Quad Core 2.8GHz Processor (Turbo up to 3.8GHz)\r\n\r\n<b>RAM:</b> 8GB DDR4 2400MHz RAM\r\n\r\n<b>Storage:</b> 256GB SSD\r\n\r\n<b>Graphics:</b> NVIDIA GeForce GTX 1060 6GB DDR5 VRAM\r\n\r\n<b>Display:</b> 13.3 Inch Full HD IPS Anti-Glare Display with Brightness up to 300 nits\r\n\r\n<b>Operating System:</b> Windows 10 64-bit OS\r\n\r\n<b>Battery Life:</b> Up to 5 hours\r\n\r\n<b>Weight:</b> 5.8 lbs\r\n\r\n<b>Upgrading Options:</b> RAM - Up to 32GB | Storage Upgradable', '1439.9900', 'https://laptopunderbudget.com/wp-content/uploads/2017/06/Alienware-13-R3-2017.jpg', 'MR44CB', 8, '2017-11-26 10:18:14', '2017-11-26 14:42:00'),
(3, 0, 'MSI GS43VR Phantom Pro-069 14″ Ultraportable Laptop', '<b>Processor:</b> Intel Core i7-7700HQ Quad Core 2.8GHz Processor (Turbo up to 3.8GHz)\r\n\r\n<b>RAM:</b> 16GB DDR4 2400MHz RAM\r\n\r\n<b>Storage:</b> 1TB 7200RPM HDD + 128GB SSD\r\n\r\n<b>Graphics:</b> NVIDIA GeForce GTX 1060 6GB DDR5 VRAM\r\n\r\n<b>Display:</b> 14.3 Inch Non Reflective IPS Display with 1920 x 1080 Resolution\r\n\r\n<b>Operating System:</b> Windows 10 64-bit OS\r\n\r\n<b>Battery Life:</b> Up to 5 hours\r\n\r\n<b>Weight:</b> 3.75 lbs\r\n\r\n<b>Upgrading Options:</b> RAM - Up to 32GB | Storage Upgradable', '1449.9700', 'https://laptopunderbudget.com/wp-content/uploads/2016/07/MSI-GS43VR-Phantom-Pro-006-14-Inch-Gaming-Laptop.jpg', 'MBD54', 23, '2017-11-26 10:32:24', '2017-11-26 14:42:21'),
(4, 0, 'Acer Aspire VX15', '<b>Processor:</b> Intel Core i7-7700HQ Quad Core 2.8GHz Processor (Turbo up to 3.8GHz)\r\n\r\n<b>RAM:</b> 16GB DDR4 RAM\r\n\r\n<b>Storage:</b> 256GB SSD\r\n\r\n<b>Graphics:</b> NVIDIA GeForce GTX 1050Ti 4GB DDR5 VRAM\r\n\r\n<b>Display:</b> 15.6 Inch Full HD Widescreen IPS Display (1920 x 1080)\r\n\r\n<b>Operating System:</b> Windows 10 64-bit OS\r\n\r\n<b>Battery Life:</b> Up to 6 hours\r\n\r\n<b>Weight:</b> 5.51 lbs\r\n\r\n<b>Upgrading Options:</b> RAM - Up to 32GB | Empty storage bay to add HDD or SSD', '1480.0600', 'https://laptopunderbudget.com/wp-content/uploads/2016/10/Acer-Aspire-VX-15-Gaming-Laptop-e1491115093500.jpg', 'BT98D3', 2, '2017-11-26 10:44:50', '2017-11-26 14:42:46'),
(5, 0, 'Dell Inspiron i5767 17.3″ Gaming Laptop', ' Intel Core i7-7500U Dual Core 2.50GHz Processor (Turbo up to 3.5GHz)\r\n\r\n<b>RAM:</b> 16GB DDR4 2400MHz RAM\r\n\r\n<b>Storage:</b> 2TB Hard Disk\r\n\r\n<b>Graphics:</b> AMD Radeon R7 M445 4GB DDR5 Graphics\r\n\r\n<b>Display:</b> 17.3 Inch Full HD Anti-Glare LED-Backlit Display with 1920 x 1080 Resolution\r\n\r\n<b>Operating System:</b> Windows 10 Home 64-bit OS\r\n\r\n<b>Battery Life:</b> Up to 5 hours\r\n\r\n<b>Weight:</b> 6.8 lbs\r\n\r\n<b>Upgrading Options:</b> RAM - Up to 16GB', '699.0000', 'https://laptopunderbudget.com/wp-content/uploads/2017/06/Dell-Inspiron-i5765-17.3-Inch-Full-HD-Gaming-Laptop.jpg', 'BN45S3', 6, '2017-11-26 10:49:47', '2017-11-26 14:43:07');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
