<style>
    * {
        font-family: sans-serif;
    }
    .container {
        margin: 10px;
        float: left;
        width: 250px;
        border: 2px solid silver;
        font-size: .9rem;
    }
    .images {
        padding: 20px;
        border: 3px solid #e4e4e4;
        height: 170px;
    }
    .title-link {
        color: #cc0046;
        text-decoration: none !important;
    }
    .title-link h3, .price, .counter, .description {
        padding: 10px;
    }
    .price, .counter {
        color: white;
        font-weight: 600;
        text-align: right;
        font-size: 1rem;
        letter-spacing: .5px;
    }
    .price {
        background: #9a9a9a;
    }
    .counter {
        background: #cc0046;
    }
    .description {
        background: #efefef;
        font-size: .8rem;
    }
</style>
<?php

use \Shop\Products\Product;

require_once 'app/start.php';

$products = Product::find('all');

foreach ($products as $product) {
    echo '<div class="container">';
    echo '<div class="images"><a href="#"><img src="' . $product->images . '" alt="Zdjęcie produktu" width="100%"></a></div>';
    echo '<a class="title-link" href="#"><h3>' . $product->name . '</h3></a>';
    echo '<div class="price">Cena: £ ' . $product->price . '</div>';
    echo '<div class="counter">Dostępna ilość: ' . $product->counter . '</div>';
    echo '<div class="description">' . nl2br($product->description) . '</div>';
    echo '</div>';
}
